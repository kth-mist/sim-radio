#ifndef AX25_CALLSIGN_H
#define AX25_CALLSIGN_H

#include <stdint.h>

#define CALLSIGN_LENGTH 7 

void ax25_add_callsign(uint8_t *ax25_packet, uint32_t *ax25_packet_length, uint8_t *data, uint32_t data_length,
                uint8_t *toCallSign, uint8_t *fromCallSign);

#endif
