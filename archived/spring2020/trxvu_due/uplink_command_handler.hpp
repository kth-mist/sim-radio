/**
 * @file   uplink_command_handler.hpp
 * @author John Wikman
 */

#ifndef UPLINK_COMMAND_HANDLER_HPP
#define UPLINK_COMMAND_HANDLER_HPP

#define UL_DEFAULT_BITRATE (1200)

#define UL_MAX_FRAMES       (40)
#define UL_MAX_PAYLOAD_SIZE (200)
#define UL_MAX_FRAME_SIZE   (UL_MAX_PAYLOAD_SIZE + 20)

unsigned long ul_watchdog_last_kick();
bool ul_trigger_soft_reset();
bool ul_trigger_hard_reset();
unsigned int ul_bitrate();

void ul_command_handler_init(void);
void ul_command_handler_provide_frame(const uint8_t *data, size_t datalen);
void ul_command_handler_get_i2c_response(const uint8_t **buf, size_t *len);
int ul_command_handler_receive_i2c(const uint8_t *data, size_t datalen);

#endif /* UPLINK_COMMAND_HANDLER_HPP */
