/**
 * @file   ax25.hpp
 * @author John Wikman
 */

#ifndef AX25_HPP
#define AX25_HPP

#include <inttypes.h>
#include <stdlib.h>

void ax25_encode(uint8_t *dst,
                 size_t *dstlen,
                 const uint8_t *src,
                 size_t srclen,
                 const char *to_callsign,
                 const char *from_callsign);

#endif /* AX25_HPP */
