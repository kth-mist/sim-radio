#include <inttypes.h>
extern "C" {
  #include "I2C.h"
  #include "I2C_configuration.h"
  #include "trxvu_uplink_sim.h"
}

void setup() {
  Serial.begin(250000);
  pinMode(13, OUTPUT);
  I2C_start(I2C_BIT_RATE, I2C_TIMEOUT_RATE);
}


uint8_t sCommand;

int wake = 0;
bool w = false;

void loop() {

  if (wake++ == 250000) {
    if (w) {
        digitalWrite(13, LOW);
        w = false;
      }
    else {
        digitalWrite(13, HIGH);
        w = true;
      }
     wake = 0; 
  }

  if (Serial.available() > 0){
    sCommand = Serial.read();
    sCommand -= 48;

    switch (sCommand) {
      case 1 : requestFrameCount();
               break;
      case 2 : requestNextFrameLength();
               break;
      case 3 : requestNextFrame();
               break;
      case 4 : requestAllFrames();  
                break;
      default : break;
    }
  } 
}


uint8_t dataFromI2C[200];
void requestFrameCount() {

  unsigned short frameCount;
  int ret = simulatorTrxvu_rcGetFrameCount (0, &frameCount);
  Serial.print("Number of frames in buffer: ");
  Serial.println(frameCount, HEX);
}

void requestNextFrameLength() {

  uint8_t length;
  simulatorTrxvuRequestNextFrameLength(&length);
  Serial.print("Next length: ");
  Serial.println(length);
  
}

void requestAllFrames() {
  Serial.println("Fetching all frames");
  unsigned short count;
  int received = 0;
  unsigned long time = millis();
//  Serial.print("time ");
//  Serial.println(time);
  int ret = simulatorTrxvu_rcGetFrameCount(0, &count);
 // Serial.println(count);
  while (count > 0) {
    received++;
    requestNextFrame();
    ret = simulatorTrxvu_rcGetFrameCount(0, &count);
 //   Serial.println(count);
    }
  time = millis() - time;
  Serial.print("time after ");
  Serial.println(time);
  Serial.println(received);
  
  }



void requestNextFrame() {
  
  SimulatorTrxvuRxFrame rx_frame;
  int ret = simulatorTrxvu_rcGetCommandFrame (0, &rx_frame);
//  Serial.print("Length of telecommand: ");
//  Serial.println(rx_frame.rx_length);

  if (rx_frame.rx_length) {
//    Serial.println("Telecommand:");
    for(int i = 0; i < rx_frame.rx_length; i++) {
 //     Serial.print(rx_frame.rx_framedata[i], HEX);
 //     Serial.print(" ");
    }
//    Serial.println();
  }
  else
    Serial.println("no command");
}
















