#include <stdlib.h>
#include <stdint.h>
#include "I2C.h"
#include "trxvu_uplink_sim.h"

#define ENABLE_DEBUG
#include "debug.h"


uint8_t commandToRadio;
void printError( int error );
void requestNoOfTelecommands ( unsigned short* number );


int simulatorTrxvu_rcGetFrameCount (unsigned char index, unsigned short *frameCount){

  commandToRadio = GET_NO_FRAMES;
  printError(I2C_write(TRXVU_ADRESS, &commandToRadio, 1));
  printError(I2C_read(TRXVU_ADRESS, (uint8_t*) frameCount, 1));
  }

uint8_t fromI2C[200];
int simulatorTrxvu_rcGetCommandFrame (unsigned char index, SimulatorTrxvuRxFrame* rx_frame){
  uint8_t lengthToRead;
  simulatorTrxvuRequestNextFrameLength(&lengthToRead);
  rx_frame->rx_length = (unsigned short) lengthToRead;
  if (lengthToRead == 0) 
    return 0;
  
  commandToRadio = GET_FRAME_FROM_RECEIVE_BUFFER;
  printError(I2C_write(TRXVU_ADRESS, &commandToRadio, 1));
  printError(I2C_read(TRXVU_ADRESS, fromI2C, lengthToRead));
  rx_frame->rx_framedata = fromI2C;

  commandToRadio = REMOVE_FRAME_FROM_BUFFER;
  printError(I2C_write(TRXVU_ADRESS, &commandToRadio, 1));

  return 0;
}



void simulatorTrxvuRequestNextFrameLength(uint8_t* length) {
  commandToRadio = GET_FRAME_LENGTH;
  printError(I2C_write(TRXVU_ADRESS, &commandToRadio, 1));
  printError(I2C_read(TRXVU_ADRESS, length, 1));  
  }


/*
 * Request length of next telecommand. If length = 0, no telecommand in buffer
 * @param tc_length Memory location where to store the length from from the I2C slave 
 *
void requestTelecommandLength ( uint8_t* tc_length ) {
  commandToRadio = GET_FRAME_LENGTH;
  printError(I2C_write(TRXVU_ADRESS, &commandToRadio, 1));
  printError(I2C_read(TRXVU_ADRESS, tc_length, 1));  
}
*/
/*
 * Request next telecommand from the TRXVU-simulator
 
void requestTelecommand (uint8_t* tcBuffer, uint8_t* tcLength) {
  uint8_t telecommandLength;
  requestTelecommandLength( &telecommandLength );

  //Serial.print("Length of next command: ");
  //Serial.println(telecommandLength);
  
  commandToRadio = SEND_NEXT_TC;
  if (telecommandLength) {
      printError(I2C_write(TRXVU_ADRESS, &commandToRadio, 1));
      printError(I2C_read(TRXVU_ADRESS, tcBuffer, telecommandLength));
  }
  *tcLength = telecommandLength;
}*/
/*
void requestNoOfTelecommands ( unsigned short* number ) {
  command = SEND_COUNT_TC;
  printError(I2C_write(TRXVU_ADRESS, &command, 1));
  printError(I2C_read(TRXVU_ADRESS, number, 1));
}
*/
void printError (int error) {
  if (error) {
      debugln("error");
  //  Serial.print("Error: ");
  //  Serial.println(error); 
  }  
}

