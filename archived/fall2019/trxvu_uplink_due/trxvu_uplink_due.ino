

extern "C" {
  #include "debug2.h"
  //#include "I2Cslave.h"
  #include "SlaveDefinitions.h"
  #include "parser.h"
  #include "SIM.h"
  #include <Wire.h>
}


// Buffer variabels
uint8_t frameBuffer[FRAME_BUFFER_SIZE][MAX_FRAME_SIZE];
int frameSize[FRAME_BUFFER_SIZE];
uint8_t head = 0;
uint8_t tail = 0;
uint8_t noOfFrames = 0;

// KISS receiving variabels
uint8_t receiveBuffer[MAX_KISS_FRAME_SIZE];
int receiveBufferIndex = 0;
uint8_t receiveChar;
bool receiveFrame = false;

uint8_t receivedData[100];
uint8_t dataIndex=0;

//Define the TC frame data
uint8_t seud_switch_on[]={192,24,12,196,111,0,8,16,176,1,0,0,0,0,218,92};
uint8_t time_keep[]={192,24,12,196,113,0,14,16,34,1,0,0,0,3,0,5,93,246,76,233,28,219};


void setup() {
  Serial.begin(115200);
  debugSetup();

  frameSize[0]=8;
  for(int i=0;i<8;i++) frameBuffer[0][i]=i;
  
  Wire.begin(0x60);
  Wire.setClock(400000);
  Wire.onReceive(receiveCallback);
  Wire.onRequest(requestCallback);

  Serial.println("Task started\n");
}

void loop() {
  if(dataIndex>0){
    while(dataIndex>0){
      for(int i=0;i<dataIndex;i++){
        Serial.print(receivedData[i]);
        Serial.print(' ');
        dataIndex--;
      }
    }
    Serial.println();  
  }

    delay(100);
}


// Response buffer
uint8_t tcToSendBuffer[MAX_FRAME_SIZE];
uint8_t tcToSendLength;

/* 
 * Called when a message are received from I2C. 
 * In this case a commandcode on 1 byte. generates a response according to the command code
 * and puts the response and length in response buffer
 * @param data Pointer to data received
 * @param dataLength number of bytes received
 */
void receiveCallback (int dataLength) {
  uint8_t data;
  Serial.println("receiveCallback invoked");
  Serial.print("dataLength:  ");
  Serial.println(dataLength);
  
  if(dataLength == 1) {
    data=Wire.read();
    Serial.print("data:  ");
    Serial.println(data);
    switch(data) {
        case GET_NO_FRAMES :                  
                                              tcToSendBuffer[1] = 0;
                                              tcToSendBuffer[0] = 1;
                                              tcToSendLength = 2;
                                              break;
        case GET_FRAME_LENGTH :               tcToSendBuffer[0] = frameSize[tail];
                                              tcToSendLength = 1;
                                              break;
        case GET_FRAME_FROM_RECEIVER_BUFFER : //Set the data to send
                                              //for (int i = 0; i < frameSize[tail]; i++)
                                              //     tcToSendBuffer[i] = frameBuffer[tail][i];
                                              //tcToSendLength = frameSize[tail];
                                              
                                              tcToSendLength = sizeof(seud_switch_on);
                                              for(int i=0;i<tcToSendLength;i++){
                                                tcToSendLength[i]=seud_switch_on[i];
                                              }
                                              break;
        case REMOVE_FRAME_FROM_BUFFER :       if (noOfFrames > 0) {
                                                noOfFrames--;
                                                tail++;
                                                tail = tail % 40;
                                              }
        default :                             break;
      } 
    } 
}


void requestCallback () {
    Serial.println("requestCallback invoked");
    Wire.write(tcToSendBuffer,tcToSendLength);
  }
