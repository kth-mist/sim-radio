/**
 * @file kiss.hpp
 */

#ifndef KISS_HPP
#define KISS_HPP

#include <inttypes.h>
#include <stdlib.h>

void kiss_encode(uint8_t *dst,
                 size_t *dstlen,
                 const uint8_t *src,
                 size_t srclen);

bool kiss_decode_provide(uint8_t data);
void kiss_decode_fetch(const uint8_t **data, size_t *datalen);

#endif /* KISS_HPP */
