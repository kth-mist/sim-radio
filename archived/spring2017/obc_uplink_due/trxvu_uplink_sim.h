#ifndef _TRXVU_UPLINK_SIM_H
#define _TRXVU_UPLINK_SIM_H


#define TRXVU_ADRESS 0x08
#define GET_NO_FRAMES 0x21
#define GET_FRAME_LENGTH 0x23
#define GET_FRAME_FROM_RECEIVE_BUFFER 0x22
#define REMOVE_FRAME_FROM_BUFFER 0x24

typedef struct _simulator_trxvu_rx_frame{
    unsigned short rx_length;
    uint8_t* rx_framedata;
}SimulatorTrxvuRxFrame;




int simulatorTrxvu_rcGetFrameCount (unsigned char index, unsigned short *frameCount);
int simulatorTrxvu_rcGetCommandFrame (unsigned char index, SimulatorTrxvuRxFrame *rx_frame);


void simulatorTrxvuRequestNextFrameLength(uint8_t* length);

#endif
