extern "C" {
  #include "I2C_slave.h"
  #include "I2C_slave_definitions.h"
  #include "ax25_callsign.h"
  #include "kiss.h"
}

/* begin simulator configurations */

#define TRXVU_DOWNLINK_SIM_SEND_INTERVAL 100
#define TRXVU_DOWNLINK_SIM_MAX_BUFFER    40

/* end simulator configurations */

#define TRXVU_COMMAND_SEND_TM_DEF_CALLSIGN        0x10
#define TRXVU_COMMAND_SET_DEFAULT_TO_CALLSIGN     0x22
#define TRXVU_COMMAND_SET_DEFAULT_FROM_CALLSIGN   0x23

#define TRXVU_ERR_DOWNLINK_BUFFER_FULL            255
#define TRXVU_ERR_INVALID_CALLSIGN                254
#define TRXVU_ERR_INVALID_COMMAND                 253
#define TRXVU_ERR_PACKET_TOO_LARGE                252

uint8_t  response_data[255];
uint32_t response_length;

uint8_t kiss_packet[511];
uint32_t kiss_packet_length;

uint8_t ToCallsign[CALLSIGN_LENGTH] = "MISTGS";   // start up destination callsign
uint8_t FromCallsign[CALLSIGN_LENGTH] = "MIST01";  // start up source callsign

uint8_t ax25_frame_buffer[40][255];
uint32_t ax25_frame_length[40];

int head = 0;
int tail = 0;

void setup() {
  SerialUSB.begin(19200);
  Serial.begin(19200);
  I2C_setup(i2c_receive_cb, i2c_transmit_begin_cb, i2c_transmit_end_cb);
}

void loop() {
  // check if the buffer is not empty, then send it to GS if not
  while(head != tail) {
    int nextTail = tail+1;

    if(nextTail >= TRXVU_DOWNLINK_SIM_MAX_BUFFER){
      nextTail = 0;
    }

    kiss_encode(kiss_packet, &kiss_packet_length, ax25_frame_buffer[tail], ax25_frame_length[tail]);

    Serial.write(kiss_packet, kiss_packet_length);
    SerialUSB.print("SENDING to GS. Length: ");
    SerialUSB.println(kiss_packet_length);

    for(int i=0; i < kiss_packet_length; i++) {
      SerialUSB.print(kiss_packet[i], HEX);
      SerialUSB.print(" ");
    }

    SerialUSB.println(" ");

    tail = nextTail;
  }

  // wait for send interval to send downlink buffer to GS
  delay(TRXVU_DOWNLINK_SIM_SEND_INTERVAL);
}


void sendTMdefCallsign(uint8_t *telemetry, uint32_t len) {

  int nextHead = head + 1;

    if(nextHead >= TRXVU_DOWNLINK_SIM_MAX_BUFFER) {
        nextHead = 0;
    }

    // buffer full
    if(nextHead == tail) {
        response_length = 1;
        response_data[0] = TRXVU_ERR_DOWNLINK_BUFFER_FULL;
        return;
    }

    ax25_add_callsign(ax25_frame_buffer[head], &ax25_frame_length[head], telemetry, len, ToCallsign, FromCallsign);

    response_length = 1;
    if((nextHead - tail) > 0 )
      response_data[0] = TRXVU_DOWNLINK_SIM_MAX_BUFFER - (nextHead - tail);
    else
      response_data[0] = (tail - nextHead);
  
      
    head = nextHead;
}

void setDefaultToCallsign(uint8_t *callsign, uint32_t len) {
  
  for(int i = 0; i < len; i++){
    ToCallsign[i] = callsign[i];
    response_data[i] = callsign[i];
  }

  response_length = len;
}

void setDefaultFromCallsign(uint8_t *callsign, uint32_t len) {

  for (int i = 0; i < len; i++) {
    FromCallsign[i] = callsign[i];
    response_data[i] = callsign[i];
  }

  response_length = len;
}

/* begin i2c callbacks */

void i2c_receive_cb(uint8_t *data, uint32_t len) {
  uint8_t commandCode = data[0];

  if(commandCode == TRXVU_COMMAND_SEND_TM_DEF_CALLSIGN){

    sendTMdefCallsign(data+1, len-1);
    
  } else if(commandCode == TRXVU_COMMAND_SET_DEFAULT_TO_CALLSIGN) {
    
    setDefaultToCallsign(data+1, len-1);

  } else if(commandCode == TRXVU_COMMAND_SET_DEFAULT_FROM_CALLSIGN) {

    setDefaultFromCallsign(data+1, len-1);

  } else {
    
    response_data[0] = TRXVU_ERR_INVALID_COMMAND;
    response_length = 1;
  
  }
  
}

void i2c_transmit_begin_cb() {

  currentPacket.MSP_packet = response_data;
  currentPacket.packetSize = response_length;
  
}

void i2c_transmit_end_cb() {
  
}

/* end i2c callbacks */

