/**
 * @file   utils.cpp
 * @author John Wikman
 */

#include <inttypes.h>

#include "utils.hpp"

/**
 * Encodes the entered value into dst in little-endian byte order.
 *
 * @warning Assumes valid input.
 */
void u16tole(uint8_t *dst, uint16_t value)
{
	dst[0] = (uint8_t) (value & 0xff);
	dst[1] = (uint8_t) ((value >> 8) & 0xff);
}

/**
 * Encodes the entered value into dst in big-endian byte order.
 *
 * @warning Assumes valid input.
 */
void u16tobe(uint8_t *dst, uint16_t value)
{
	dst[0] = (uint8_t) ((value >> 8) & 0xff);
	dst[1] = (uint8_t) (value & 0xff);
}

/**
 * Encodes the entered value into dst in little-endian byte order.
 *
 * @warning Assumes valid input.
 */
void u32tole(uint8_t *dst, uint32_t value)
{
	dst[0] = (uint8_t) (value & 0xff);
	dst[1] = (uint8_t) ((value >> 8) & 0xff);
	dst[2] = (uint8_t) ((value >> 16) & 0xff);
	dst[3] = (uint8_t) ((value >> 24) & 0xff);
}

/**
 * Encodes the entered value into dst in big-endian byte order.
 *
 * @warning Assumes valid input.
 */
void u32tobe(uint8_t *dst, uint32_t value)
{
	dst[0] = (uint8_t) ((value >> 24) & 0xff);
	dst[1] = (uint8_t) ((value >> 16) & 0xff);
	dst[2] = (uint8_t) ((value >> 8) & 0xff);
	dst[3] = (uint8_t) (value & 0xff);
}

/**
 * Encodes the value in src from little-endian byte order.
 *
 * @warning Assumes valid input.
 */
uint32_t letou32(const uint8_t *src)
{
	uint32_t value = (src[0])       |
	                 (src[1] << 8)  |
	                 (src[2] << 16) |
	                 (src[3] << 24);

	return value;
}

/**
 * Encodes the value in src from big-endian byte order.
 *
 * @warning Assumes valid input.
 */
uint32_t betou32(const uint8_t *src)
{
	uint32_t value = (src[0] << 24) |
	                 (src[1] << 16) |
	                 (src[2] << 8)  |
	                 (src[3]);

	return value;
}
