/**
 * @file   downlink_command_handler.cpp
 * @author John Wikman
 */

#include <Arduino.h>

#include <inttypes.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <wiring.h>

#include "ax25.hpp"
#include "downlink_command_codes.hpp"
#include "downlink_command_handler.hpp"
#include "utils.hpp"

static uint8_t tx_buffer[DL_MAX_FRAMES][DL_MAX_FRAME_SIZE];
static size_t tx_buffer_length[DL_MAX_FRAMES];
static size_t tx_head;
static size_t tx_bufcount;

static uint8_t beacon_buffer[DL_MAX_FRAME_SIZE];
static size_t beacon_length;
static unsigned long beacon_interval;

static char to_callsign[7];
static char from_callsign[7];

static uint8_t response_data[DL_MAX_FRAME_SIZE];
static size_t response_length;

/* Time in milliseconds since the downlink was initialized */
static unsigned long starttime;

/* Time (in milliseconds since startup) of last watchdog reset */
static unsigned long watchdog_reset_time;

/* Boolean variable specifying a soft reset */
static bool indicator_soft_reset;

/* Boolean variable specifying a hard reset */
static bool indicator_hard_reset;

/* Boolean variable specifying if transmitter should be turned on during idle */
static bool indicator_idle_state_on;

/* Radio bitrate in bits per second. Should be used to determine the delay period in main loop. */
static unsigned int bitrate;

unsigned long dl_watchdog_last_kick()
{
	return watchdog_reset_time;
}

bool dl_trigger_soft_reset()
{
	return indicator_soft_reset;
}

bool dl_trigger_hard_reset()
{
	return indicator_hard_reset;
}

bool dl_idle_state_on()
{
	return indicator_idle_state_on;
}

unsigned int dl_bitrate()
{
	return bitrate;
}

/**
 * Initializes/resets the downlink command handler.
 */
void dl_command_handler_init(void)
{
	tx_head = 0;
	tx_bufcount = 0;
	beacon_length = 0;
	beacon_interval = 0;
	strcpy(to_callsign, DL_DEFAULT_TO_CALLSIGN);
	strcpy(from_callsign, DL_DEFAULT_FROM_CALLSIGN);
	response_length = 0;
	starttime = millis();
	watchdog_reset_time = millis();
	indicator_soft_reset = false;
	indicator_hard_reset = false;
	indicator_idle_state_on = false;
	bitrate = DL_DEFAULT_BITRATE;
}

void dl_command_handler_poll_frame(const uint8_t **buf, size_t *len)
{
	if (tx_bufcount == 0) {
		*buf = NULL;
		*len = 0;
	} else {
		*buf = tx_buffer[tx_head];
		*len = tx_buffer_length[tx_head];
		tx_head++;
		if (tx_head >= DL_MAX_FRAMES)
			tx_head = 0;

		tx_bufcount--;
	}
}

void dl_command_handler_get_i2c_response(const uint8_t **buf, size_t *len)
{
	if (response_length == 0) {
		*buf = NULL;
		*len = 0;
	} else {
		*buf = response_data;
		*len = response_length;
	}
}

/**
 * Handle incoming I2C transmissions to TRXVU downlink. First byte of received
 * data indicates an opcode.
 */
int dl_command_handler_receive_i2c(const uint8_t *data, size_t datalen)
{
	int ret = 0;
	uint8_t command_code = data[0];
	size_t slot = (tx_head + tx_bufcount) % DL_MAX_FRAMES;
	unsigned long uptime;

	switch (command_code) {
	case DLCC_WATCHDOG_RESET:
		watchdog_reset_time = millis();
		response_length = 0;
		break;
	case DLCC_SOFTWARE_RESET:
		indicator_soft_reset = true;
		response_length = 0;
		break;
	case DLCC_HARDWARE_SYSTEM_RESET:
		indicator_hard_reset = true;
		response_length = 0;
		break;
	case DLCC_SEND_FRAME:
		/* TODO: Describe format here and check length */
		if (tx_bufcount >= DL_MAX_FRAMES) {
			response_data[0] = 0xFF;
			response_length = 1;
		} else {
			ax25_encode(tx_buffer[slot],
			            &tx_buffer_length[slot],
			            &data[1],
			            datalen - 1,
			            to_callsign,
			            from_callsign);
			tx_bufcount++;

			response_data[0] = (uint8_t) ((DL_MAX_FRAMES - tx_bufcount) & 0xff);
			response_length = 1;
		}
		break;
	case DLCC_SEND_FRAME_OVERRIDE_CALLSIGN:
		/* TODO: Describe format here and check length */
		if (tx_bufcount >= DL_MAX_FRAMES) {
			response_data[0] = 0xFF;
			response_length = 1;
		} else {
			ax25_encode(tx_buffer[slot],
			            &tx_buffer_length[slot],
			            &data[15],
			            datalen - 15,
			            (const char *) &data[1],
			            (const char *) &data[8]);
			tx_bufcount++;

			response_data[0] = (uint8_t) ((DL_MAX_FRAMES - tx_bufcount) & 0xff);
			response_length = 1;
		}
		break;
	case DLCC_SET_BEACON:
		/* TODO: Describe format here and check length */
		beacon_interval = data[1];
		beacon_interval |= data[2] & 0x0f;
		if (beacon_interval > 3000)
			beacon_interval = 3000;

		ax25_encode(beacon_buffer,
		            &beacon_length,
		            &data[3],
		            datalen - 3,
		            to_callsign,
		            from_callsign);

		response_length = 0;
		break;
	case DLCC_SET_BEACON_OVERRIDE_CALLSIGN:
		/* TODO: Describe format here and check length */
		beacon_interval = data[1];
		beacon_interval |= data[2] & 0x0f;
		if (beacon_interval > 3000)
			beacon_interval = 3000;

		ax25_encode(beacon_buffer,
		            &beacon_length,
		            &data[17],
		            datalen - 17,
		            (const char *) &data[3],
		            (const char *) &data[10]);

		response_length = 0;
		break;
	case DLCC_CLEAR_BEACON:
		beacon_length = 0;
		beacon_interval = 0;
		response_length = 0;
		break;
	case DLCC_SET_DEFAULT_TO_CALLSIGN:
		/* TODO: Describe format here and check length */
		memcpy(to_callsign, &data[1], 7);
		response_length = 0;
		break;
	case DLCC_SET_DEFAULT_FROM_CALLSIGN:
		/* TODO: Describe format here and check length */
		memcpy(from_callsign, &data[1], 7);
		response_length = 0;
		break;
	case DLCC_SET_TRANSMITTER_IDLE_STATE:
		/* TODO: Describe format here and check length */
		if ((data[0] & 0x01) == 0x01)
			indicator_idle_state_on = true;
		else
			indicator_idle_state_on = false;
		
		response_length = 0;
		break;
	case DLCC_MEASURE_ALL_TELEMETRY_CHANNELS:
		/* TODO: Provide a non-zeroized implementation */
		memset(response_data, 0x00, 12);
		response_length = 12;
		break;
	case DLCC_MEASURE_LAST_TELEMETRY_CHANNELS:
		/* TODO: Provide a non-zeroized implementation */
		memset(response_data, 0x00, 12);
		response_length = 12;
		break;
	case DLCC_SET_BITRATE:
		/* TODO: Describe format here and check length */
		switch (data[1]) {
		case 0x01:
			bitrate = 1200;
			break;
		case 0x02:
			bitrate = 2400;
			break;
		case 0x04:
			bitrate = 4800;
			break;
		case 0x08:
			bitrate = 9600;
			break;
		default:
			break;
		}
		response_length = 0;
		break;
	case DLCC_REPORT_TRANSMITTER_UPTIME:
		/* TODO: Describe format here and check length */
		uptime = (millis() - starttime) / 1000;
		u32tole(&response_data[0], (uint32_t) uptime);
		response_length = 4;
		break;
	case DLCC_REPORT_TRANSMITTER_STATE:
		/* TODO: Describe format here and check length */
		response_data[0] = 0;
		if (indicator_idle_state_on)
			response_data[0] |= 0x01;
		if (beacon_length > 0)
			response_data[0] |= 0x02;

		switch (bitrate) {
		case 2400:
			response_data[0] |= 0x04;
			break;
		case 4800:
			response_data[0] |= 0x08;
			break;
		case 9600:
			response_data[0] |= 0x0c;
			break;
		default:
			break;
		}

		response_length = 1;
		break;
	default:
		ret = 1;
		break;
	}

	return ret;
}
