

extern "C" {
  #include "debug2.h"
  #include "SlaveDefinitions.h"
  #include "parser.h"
  #include "SIM.h"
  #include <Wire.h>
}


// Buffer variabels
uint8_t frameBuffer[FRAME_BUFFER_SIZE][MAX_FRAME_SIZE];
int frameSize[FRAME_BUFFER_SIZE];
uint8_t head = 0;
uint8_t tail = 0;
uint8_t noOfFrames = 0;

// KISS receiving variabels
uint8_t receiveBuffer[MAX_KISS_FRAME_SIZE];
int receiveBufferIndex = 0;
uint8_t receiveChar;
bool receiveFrame = false;

uint8_t receivedData[100];
uint8_t dataIndex=0;
void setup() {
  Serial.begin(115200);
  debugSetup();

  frameSize[0]=8;
  for(int i=0;i<8;i++) frameBuffer[0][i]=i;
  
  Wire.begin(0x61);
  Wire.setClock(400000);
  Wire.onReceive(receiveCallback);
  Wire.onRequest(requestCallback);

  Serial.println("Task started");
}

void loop() {
  if(dataIndex>0){
    while(dataIndex>0){
      for(int i=0;i<dataIndex;i++){
        Serial.print(receivedData[i]);
        Serial.print(' ');
        dataIndex--;
      }
    }
    Serial.println();  
  }
    delay(100);
}


// Response buffer
uint8_t tcToSendBuffer[MAX_FRAME_SIZE];
uint8_t tcToSendLength;

/* 
 * Called when a message are received from I2C. 
 * In this case a commandcode on 1 byte. generates a response according to the command code
 * and puts the response and length in response buffer
 * @param data Pointer to data received
 * @param dataLength number of bytes received
 */
void receiveCallback (int dataLength) {
  uint8_t data;
  Serial.println("receiveCallback invoked");
  while(Wire.available()){
    receivedData[dataIndex++]=Wire.read();
  }
  Serial.print("dataLength:  ");
  Serial.println(dataLength);

  return;
  /*The downlink simulator only receive data from OBC*/


  // if(dataLength == 1) {
  //   data=Wire.read();
  //   switch(data) {
  //       case GET_NO_FRAMES :                  tcToSendBuffer[0] = noOfFrames;
  //                                             tcToSendLength = 1;
  //                                             break;
  //       case GET_FRAME_LENGTH :               tcToSendBuffer[0] = frameSize[tail];
  //                                             tcToSendLength = 1;
  //                                             break;
  //       case GET_FRAME_FROM_RECEIVER_BUFFER : for (int i = 0; i < frameSize[tail]; i++)
  //                                                  tcToSendBuffer[i] = frameBuffer[tail][i];
  //                                             tcToSendLength = frameSize[tail];
  //                                             break;
  //       case REMOVE_FRAME_FROM_BUFFER :       if (noOfFrames > 0) {
  //                                               noOfFrames--;
  //                                               tail++;
  //                                               tail = tail % 40;
  //                                             }
  //       default :                             break;
  //     } 
  //   } 
}

void requestCallback () {
    Serial.println("requestCallback invoked");
    while(Wire.available()){
      receivedData[dataIndex++]=Wire.read();
    }
    return;
    Wire.write(tcToSendBuffer,tcToSendLength);
  }

