#include <stdint.h>
#include <stdlib.h>

#include "ax25_callsign.h"


void ax25_add_callsign(uint8_t *ax25_packet, uint32_t* ax25_packet_length, uint8_t* data, uint32_t data_length, uint8_t *toCallSign, uint8_t *fromCallSign) {

  uint32_t i;

  // calculating the length of ax25 packet
  *ax25_packet_length = data_length + 7 + 7 + 1 + 1;

  // add destination callsign
  for ( i = 0 ; i < 6; i++ ) {
    ax25_packet[i] = (uint8_t) toCallSign[ i ] << 1;
  }

  // add ssid
  ax25_packet[6] = 0x60;

  // add source callsign
  for ( i = 7 ; i < 13 ; i++ ){
    ax25_packet[i] = (uint8_t) fromCallSign[ i - 7 ] << 1;
  }

  ax25_packet[13] = 0x62;

  ax25_packet[14] = 0x03;  // control bits
  ax25_packet[15] = 0xF0;  // protocol identifier

  // adding data
  for ( i = 0 ; i < data_length ; i ++ ){
    ax25_packet[ i + 16 ] = data[i];
  }

}
