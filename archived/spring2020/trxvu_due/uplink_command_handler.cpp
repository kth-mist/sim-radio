/**
 * @file   uplink_command_handler.cpp
 * @author John Wikman
 */

#include <Arduino.h>

#include <inttypes.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <wiring.h>

#include "debug.hpp"
#include "uplink_command_codes.hpp"
#include "uplink_command_handler.hpp"
#include "utils.hpp"

static uint8_t rx_buffer[UL_MAX_FRAMES][UL_MAX_FRAME_SIZE];
static size_t rx_buffer_length[UL_MAX_FRAMES];
static size_t rx_head = 0;
static size_t rx_bufcount = 0;

static uint8_t response_data[UL_MAX_FRAME_SIZE];
static size_t response_length;

/* Time in milliseconds since the uplink was initialized */
static unsigned long starttime;

/* Time (in milliseconds since startup) of last watchdog reset */
static unsigned long watchdog_reset_time;

/* Boolean variable specifying a soft reset */
static bool indicator_soft_reset;

/* Boolean variable specifying a hard reset */
static bool indicator_hard_reset;

/* Uplink bitrate in bits per second. Should be used to determine the poll rate in main loop. */
static unsigned int bitrate;

unsigned long ul_watchdog_last_kick()
{
	return watchdog_reset_time;
}

bool ul_trigger_soft_reset()
{
	return indicator_soft_reset;
}

bool ul_trigger_hard_reset()
{
	return indicator_hard_reset;
}

unsigned int ul_bitrate()
{
	return bitrate;
}

/**
 * Initializes/resets the state of the uplink handler.
 */
void ul_command_handler_init(void)
{
	rx_head = 0;
	rx_bufcount = 0;
	response_length = 0;
	starttime = millis();
	watchdog_reset_time = millis();
	indicator_soft_reset = false;
	indicator_hard_reset = false;
	bitrate = UL_DEFAULT_BITRATE;
}

void ul_command_handler_provide_frame(const uint8_t *data, size_t datalen)
{
	size_t slot = (rx_head + rx_bufcount) % UL_MAX_FRAMES;

	if (rx_bufcount >= UL_MAX_FRAMES)
		return;

	if ((datalen - 16) > UL_MAX_FRAME_SIZE)
		return;

	// Strip off general AX.25 header (but include secondary AX.25 header)
	// The general AX.25 header does not contain flags or the FCS
	memcpy(rx_buffer[slot], &data[16], datalen - 16);
	rx_buffer_length[slot] = datalen - 16;

	rx_bufcount++;
}

void ul_command_handler_get_i2c_response(const uint8_t **buf, size_t *len)
{
	if (response_length == 0) {
		*buf = NULL;
		*len = 0;
	} else {
		*buf = response_data;
		*len = response_length;
	}
}

int ul_command_handler_receive_i2c(const uint8_t *data, size_t datalen)
{
	int ret = 0;
	uint8_t command_code = data[0];
	size_t slot = (rx_head + rx_bufcount) % UL_MAX_FRAMES;
	unsigned long uptime;

	switch (command_code) {
	case ULCC_WATCHDOG_RESET:
		watchdog_reset_time = millis();
		response_length = 0;
		break;
	case ULCC_SOFTWARE_RESET:
		indicator_soft_reset = true;
		response_length = 0;
		break;
	case ULCC_HARDWARE_SYSTEM_RESET:
		indicator_hard_reset = true;
		response_length = 0;
		break;
	case ULCC_GET_FRAME_COUNT:
		/* TODO: Describe format here and check length */
		u16tole(&response_data[0], rx_bufcount);
		response_length = 2;
		break;
	case ULCC_GET_FRAME:
		/* TODO: Describe format here and check length */
		if (rx_bufcount == 0) {
			u16tole(&response_data[0], 0);
			response_length = 2;
		} else {
			u16tole(&response_data[0], rx_buffer_length[rx_head]);
			u16tole(&response_data[2], 0); // doppler frequency
			u16tole(&response_data[4], 0); // RSSI
			memcpy(&response_data[6], rx_buffer[rx_head], rx_buffer_length[rx_head]);
			response_length = rx_buffer_length[rx_head] + 6;
		}
		break;
	case ULCC_REMOVE_FRAME:
		/* TODO: Describe format here and check length */
		if (rx_bufcount > 0) {
			rx_head = (rx_head + 1) % UL_MAX_FRAMES;
			rx_bufcount--;
		}
		response_length = 0;
		break;
	case ULCC_MEASURE_ALL_TELEMETRY_CHANNELS:
		/* TODO: Describe format here and check length */
		u16tole(&response_data[0], 1942); // receiver doppler offset
		u16tole(&response_data[2], 179); // total supply current
		u16tole(&response_data[4], 2871); // power bus voltage
		u16tole(&response_data[6], 2245); // local oscillator temperature
		u16tole(&response_data[8], 2240); // power amplifier temp
		u16tole(&response_data[10], 1684); // receiver signal strength (rssi)
		response_length = 12;
		break;
	case ULCC_REPORT_RECEIVER_UPTIME:
		uptime = (millis() - starttime) / 1000;
		u32tole(&response_data[0], (uint32_t) uptime);
		response_length = 4;
		break;
	default:
		response_length = 0;
		ret = 1;
		break;
	}

	return ret;
}
