/**
 * @file   downlink_command_handler.hpp
 * @author John Wikman
 */

#ifndef DOWNLINK_COMMAND_HANDLER_HPP
#define DOWNLINK_COMMAND_HANDLER_HPP

#include <inttypes.h>
#include <stdlib.h>

#define DL_DEFAULT_BITRATE       (1200)
#define DL_DEFAULT_FROM_CALLSIGN ("MIST")
#define DL_DEFAULT_TO_CALLSIGN   ("SA0SAT")

#define DL_MAX_FRAMES       (40)
#define DL_MAX_PAYLOAD_SIZE (235)
#define DL_MAX_FRAME_SIZE   (DL_MAX_PAYLOAD_SIZE + 20)

unsigned long dl_watchdog_last_kick();
bool dl_trigger_soft_reset();
bool dl_trigger_hard_reset();
bool dl_idle_state_on();
unsigned int dl_bitrate();

void dl_command_handler_init(void);
void dl_command_handler_poll_frame(const uint8_t **buf, size_t *len);
void dl_command_handler_get_i2c_response(const uint8_t **buf, size_t *len);
int dl_command_handler_receive_i2c(const uint8_t *data, size_t datalen);

#endif /* DOWNLINK_COMMAND_HANDLER_HPP */
